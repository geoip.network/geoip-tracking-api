import uuid
from typing import List, Set

import lmdb  # type: ignore

from models import Pixel, VisitPixel, KeyStats, Token, VisitToken

Ki = 1024
Mi = 1024 * Ki
Gi = 1024 * Mi
MILLISECONDS = 1000  # Convert from seconds to milliseconds


class Database:
    def __init__(self):
        self.env = lmdb.Environment(
            "../eyeball.lmdb",
            map_size=8 * Gi,
            subdir=True,
            readonly=False,
            metasync=True,
            sync=True,
            map_async=False,
            mode=493,
            create=True,
            readahead=True,
            writemap=False,
            meminit=True,
            max_readers=16,
            max_dbs=32,
            max_spare_txns=1,
            lock=True,
        )
        self.pixel_db = self.env.open_db("pixel".encode())
        self.pixel_by_owner_index = self.env.open_db(
            "pixel_by_owner".encode(), dupsort=True
        )
        self.pixel_by_uuid_index = self.env.open_db("pixel_by_uuid".encode())
        self.token_db = self.env.open_db("token".encode())
        self.token_by_owner_index = self.env.open_db(
            "token_by_owner".encode(), dupsort=True
        )
        self.visit_pixel_db = self.env.open_db("visit".encode(), dupsort=True)
        self.visit_token_db = self.env.open_db("visit_token".encode(), dupsort=True)

    def write_txn(self):
        return self.env.begin(write=True)

    def read_txn(self):
        return self.env.begin()

    def update_stats(self, txn, keystats: KeyStats):
        txn.put("keystats".encode(), keystats)

    def get_stats(self, txn):
        with txn.cursor() as cursor:
            if cursor.set_key("keystats".encode()):
                return KeyStats.from_buffer_copy(cursor.value())
        return KeyStats(PixelUID=0, TokenUID=0)

    def owners_all(self, txn) -> Set[str]:
        output: Set[str] = set()
        with txn.cursor(self.pixel_db) as cursor:
            for _, value in cursor:
                pixel = Pixel.from_buffer_copy(value)
                output.add(pixel.OwnerID.decode())
        with txn.cursor(self.token_db) as cursor:
            for _, value in cursor:
                token = Token.from_buffer_copy(value)
                output.add(token.OwnerID.decode())
        return output

    def pixel_all(self, txn) -> List[Pixel]:
        output: List[Pixel] = []
        with txn.cursor(self.pixel_db) as cursor:
            for _, value in cursor:
                pixel = Pixel.from_buffer_copy(value)
                output.append(pixel)
        return output

    def pixel_put(self, txn, pixel: Pixel):
        with txn.cursor(self.pixel_db) as cursor:
            if cursor.set_key(pixel.UID.to_bytes(8, byteorder="big")):
                raise KeyError("Conflict")
        txn.put(pixel.UID.to_bytes(8, byteorder="big"), bytes(pixel), db=self.pixel_db)
        txn.put(
            pixel.OwnerID,
            pixel.UID.to_bytes(8, byteorder="big"),
            db=self.pixel_by_owner_index,
        )
        txn.put(
            pixel.UUID,
            pixel.UID.to_bytes(8, byteorder="big"),
            db=self.pixel_by_uuid_index,
        )

    def pixel_get(self, txn, uid: int) -> Pixel:
        with txn.cursor(self.pixel_db) as cursor:
            if cursor.set_key(uid.to_bytes(8, byteorder="big")):
                pixel = Pixel.from_buffer_copy(cursor.value())
                return pixel
        return None

    def pixel_delete(self, txn, uid: int):
        with txn.cursor(self.pixel_db) as pixel_cursor:
            if not pixel_cursor.set_key(uid.to_bytes(8, byteorder="big")):
                raise KeyError("pixel not found")
            pixel = Pixel.from_buffer_copy(pixel_cursor.value())
            with txn.cursor(self.pixel_by_owner_index) as owner_cursor:
                if owner_cursor.set_key(pixel.OwnerID):
                    for uid in owner_cursor.iternext_dup():
                        if uid == pixel.UID:
                            owner_cursor.delete()
                            break
            with txn.cursor(self.pixel_by_uuid_index) as uuid_cursor:
                if uuid_cursor.set_key(pixel.UUID):
                    for uid in uuid_cursor.iternext_dup():
                        if uid == pixel.UID:
                            uuid_cursor.delete()
                            break
            with txn.cursor(self.visit_pixel_db) as visit_cursor:
                if visit_cursor.set_key(pixel.UID.to_bytes(8, byteorder="big")):
                    for visit_bytes in visit_cursor.iternext_dup():
                        visit_cursor.delete()
            pixel_cursor.delete()

    def pixel_get_by_owner(self, txn, owner_uid: str) -> List[Pixel]:
        pixels = []
        with txn.cursor(self.pixel_by_owner_index) as cursor:
            if cursor.set_key(owner_uid.encode()):
                for uid in cursor.iternext_dup():
                    result = self.pixel_get(txn, int.from_bytes(uid, "big"))
                    if result is not None:
                        pixels.append(result)
            return pixels

    def pixel_get_by_uuid(self, txn, aUUID: str) -> Pixel:
        with txn.cursor(self.pixel_by_uuid_index) as cursor:
            if cursor.set_key(uuid.UUID(aUUID).bytes):
                uid = cursor.value()
                return self.pixel_get(txn, int.from_bytes(uid, "big"))
        return None

    def visit_all_for_pixeluid(self, txn, uid: int) -> List[VisitPixel]:
        with txn.cursor(self.visit_pixel_db) as cursor:
            if cursor.set_key(uid.to_bytes(8, byteorder="big")):
                for visit_bytes in cursor.iternext_dup():
                    visit = VisitPixel.from_buffer_copy(visit_bytes)
                    yield visit

    def visit_put(self, txn, visit: VisitPixel):
        txn.put(
            visit.PixelUID.to_bytes(8, byteorder="big"),
            bytes(visit),
            db=self.visit_pixel_db,
        )

    def token_all(self, txn) -> List[Token]:
        output: List[Token] = []
        with txn.cursor(self.token_db) as cursor:
            for _, value in cursor:
                token = Token.from_buffer_copy(value)
                output.append(token)
        return output

    def token_put(self, txn, token: Token):
        with txn.cursor(self.token_db) as cursor:
            if cursor.set_key(token.UID.to_bytes(8, byteorder="big")):
                raise KeyError("Conflict")
        txn.put(token.UID.to_bytes(8, byteorder="big"), bytes(token), db=self.token_db)
        txn.put(
            token.OwnerID,
            token.UID.to_bytes(8, byteorder="big"),
            db=self.token_by_owner_index,
        )

    def token_get(self, txn, uid: int) -> Token:
        with txn.cursor(self.token_db) as cursor:
            if cursor.set_key(uid.to_bytes(8, byteorder="big")):
                token = Token.from_buffer_copy(cursor.value())
                return token
        return None

    def token_delete(self, txn, uid: int) -> Token:
        with txn.cursor(self.token_db) as token_cursor:
            if not token_cursor.set_key(uid.to_bytes(8, byteorder="big")):
                raise KeyError("token not found")
            token = Token.from_buffer_copy(token_cursor.value())
            with txn.cursor(self.token_by_owner_index) as owner_cursor:
                if owner_cursor.set_key(token.OwnerID):
                    for uid in owner_cursor.iternext_dup():
                        if uid == token.UID:
                            owner_cursor.delete()
                            break
            with txn.cursor(self.visit_pixel_db) as visit_cursor:
                if visit_cursor.set_key(uid.to_bytes(8, byteorder="big")):
                    for visit_bytes in visit_cursor.iternext_dup():
                        visit_cursor.delete()
            token_cursor.delete()

    def token_get_by_owner(self, txn, owner_uid: str) -> List[Token]:
        tokens = []
        with txn.cursor(self.token_by_owner_index) as cursor:
            if cursor.set_key(owner_uid.encode()):
                for uid in cursor.iternext_dup():
                    result = self.token_get(txn, int.from_bytes(uid, "big"))
                    if result is not None:
                        tokens.append(result)
            return tokens

    def visit_all_for_tokenuid(self, txn, uid: int) -> List[VisitToken]:
        with txn.cursor(self.visit_token_db) as cursor:
            if cursor.set_key(uid.to_bytes(8, byteorder="big")):
                for visit_bytes in cursor.iternext_dup():
                    visit = VisitToken.from_buffer_copy(visit_bytes)
                    yield visit

    def visit_token_put(self, txn, visit: VisitToken):
        txn.put(
            visit.TokenUID.to_bytes(8, byteorder="big"),
            bytes(visit),
            db=self.visit_token_db,
        )
