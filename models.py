import struct
from ctypes import Structure, c_uint64, c_char, c_bool, c_uint8
from ipaddress import ip_address
from uuid import UUID


class AsDictMixin:
    @property
    def as_dict(self):
        d = {}
        for (key, _) in self._fields_:
            if isinstance(getattr(self, key), AsDictMixin):
                d[key] = getattr(self, key).as_dict
            elif isinstance(getattr(self, key), bytes):
                rawbytes = getattr(self, key)
                if key == "UUID":
                    d[key] = str(UUID(bytes=rawbytes))
                else:
                    d[key] = rawbytes.decode()
            elif isinstance(getattr(self, key), c_uint8 * 16):
                rawbytes = getattr(self, key)
                if key == "IPAddr":
                    rawbytes = struct.pack("!BBBBBBBBBBBBBBBB", *rawbytes)
                    if rawbytes.startswith(
                        b"\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\xff\xff"
                    ):  # IPv4
                        d[key] = ip_address(rawbytes[-4:]).compressed
                    else:  # IPv6
                        d[key] = ip_address(rawbytes).compressed
            else:
                d[key] = getattr(self, key)
        return d

    def __repr__(self):
        return f"{self.__class__.__name__}({', '.join(['='.join([key, str(val)]) for key, val in self.as_dict.items()])})"


class KeyStats(Structure, AsDictMixin):
    _fields_ = [("PixelUID", c_uint64), ("TokenUID", c_uint64), ("UNUSED", c_char * 23)]


class Pixel(Structure, AsDictMixin):
    _fields_ = [
        ("UID", c_uint64),
        ("OwnerID", c_char * 1025),
        ("UUID", c_char * 17),
    ]


class Token(Structure, AsDictMixin):
    _fields_ = [
        ("UID", c_uint64),
        ("OwnerID", c_char * 1025),
        ("ApplicationName", c_char * 1025),
    ]


class VisitPixel(Structure, AsDictMixin):
    _fields_ = [
        ("PixelUID", c_uint64),
        ("Timestamp", c_uint64),
        ("IPAddr", c_uint8 * 16),
        ("DoNotTrack", c_bool),
        ("OperatingSystem", c_char * 16),  # empty if DNT
        ("Browser", c_char * 16),  # empty if DNT
        ("Language", c_char * 16),  # empty if DNT
    ]


class VisitToken(Structure, AsDictMixin):
    _fields_ = [
        ("TokenUID", c_uint64),
        ("Timestamp", c_uint64),
        ("IPAddr", c_uint8 * 16),
        ("IPPrefixLen", c_uint8),
    ]
