import re
import struct
import uuid
from datetime import datetime, timedelta
from functools import wraps
from ipaddress import ip_address, ip_network
from os import environ
from urllib.parse import urljoin

import nacl
import requests as requests
from flask import Flask, request, jsonify, _request_ctx_stack
from flask_limiter import Limiter
from jose import jwt
from nacl.signing import VerifyKey
from nacl.encoding import URLSafeBase64Encoder

from database import Database
from models import Pixel, VisitPixel, VisitToken, Token
from regex_helpers import HTML_TAGS


def get_ipaddr():  # pragma: no cover
    """
    :return: the ip address for the current request
     (or 127.0.0.1 if none found) based on the X-Forwarded-For headers.

    .. deprecated:: 0.9.2
     """
    if request.access_route:
        return request.access_route[0]
    else:
        return request.remote_addr or '127.0.0.1'


database = Database()
app = Flask(__name__, static_folder='static')
limiter = Limiter(
    app,
    key_func=get_ipaddr,
    headers_enabled=True,
    default_limits=["100000 per day", "5000 per hour"]
)

M2MAUTH_DOMAIN = environ.get("EYEBALL_AUTH_URL")
API_AUDIENCE = environ.get("EYEBALL_URL")
ALGORITHMS = ["ED25519"]
CERTS = requests.get(urljoin(M2MAUTH_DOMAIN, "/v1.0/certs")).json()  # nosemgrep
KEYS = {
    int(key): {
        "key": VerifyKey(val["public_key"], encoder=nacl.signing.encoding.URLSafeBase64Encoder),
        "issuer": val["issuer"],
        "refresh": val["refresh"]
    } for key, val in CERTS.items()}


# Error handler
class AuthError(Exception):
    def __init__(self, error, status_code):
        self.error = error
        self.status_code = status_code


@app.errorhandler(AuthError)
def handle_auth_error(ex):
    response = jsonify(ex.error)
    response.status_code = ex.status_code
    return response


# Format error response and append status code
def get_token_auth_header():
    """
    Obtains the Access Token from the Authorization Header
    """
    auth = request.headers.get("Authorization", None)
    if not auth:
        raise AuthError({"code": "authorization_header_missing",
                        "description":
                            "Authorization header is expected"}, 401)

    parts = auth.split()

    if parts[0].lower() != "bearer":
        raise AuthError({"code": "invalid_header",
                        "description":
                            "Authorization header must start with"
                            " Bearer"}, 401)
    elif len(parts) == 1:
        raise AuthError({"code": "invalid_header",
                        "description": "Token not found"}, 401)
    elif len(parts) > 2:
        raise AuthError({"code": "invalid_header",
                        "description":
                            "Authorization header must be"
                            " Bearer token"}, 401)

    token = parts[1]
    return token


def requires_auth(f):
    """Determines if the Access Token is valid
    """
    @wraps(f)
    def decorated(*args, **kwargs):
        token = get_token_auth_header()
        unverified_header = jwt.get_unverified_header(token)
        key = KEYS.get(unverified_header["kid"])
        if key is not None:
            try:
                chunks = token.split(".")
                signature = URLSafeBase64Encoder.decode(chunks[2].encode())
                key["key"].verify(".".join(chunks[:2]).encode(), signature)
            except nacl.exceptions.BadSignatureError as e:
                raise AuthError({"code": "invalid_signature",
                                 "description": "signature could not be verified"
                                 }, 401) from e
            claims = jwt.get_unverified_claims(token)
            try:
                if datetime.fromtimestamp(claims["exp"]) < datetime.now():
                    raise AuthError({"code": "token_expired",
                                    "description": "token is expired"}, 401)
                r = re.compile(r"(https?://)?"+re.escape(API_AUDIENCE)+"/?")
                if (r.match(claims["aud"]) is None) or (claims["iss"] != M2MAUTH_DOMAIN):
                    raise AuthError({"code": "invalid_claims",
                                    "description":
                                        "incorrect claims,"
                                        "please check the audience and issuer"}, 401)
            except KeyError as e:
                raise AuthError({"code": "invalid_header",
                                "description":
                                    "Unable to parse authentication"
                                    " token."}, 401) from e
            _request_ctx_stack.top.current_user = claims
            return f(*args, **kwargs)
        raise AuthError({"code": "invalid_header",
                        "description": "Unable to find appropriate key"}, 401)
    return decorated


def requires_scope(required_scope):
    """Determines if the required scope is present in the Access Token
    Args:
        required_scope (str): The scope required to access the resource
    """
    token = get_token_auth_header()
    unverified_claims = jwt.get_unverified_claims(token)
    if unverified_claims.get("scope"):
        token_scopes = unverified_claims["scope"].split()
        for token_scope in token_scopes:
            if token_scope == required_scope:
                return True
    return False


@app.route('/v1.0/token',  methods=["POST"])
@requires_auth
@limiter.exempt
def generate_token():
    if not requires_scope("create:token"):
        return {"error": "not authorized"}, 401
    if (owner_id := request.json.get("owner_id")) and (application_name := request.json.get("application_name")):
        if re.match(HTML_TAGS, application_name):
            #  todo: Log this
            return {"error": "html detected, security notified"}, 400
        with database.write_txn() as txn:
            stats = database.get_stats(txn)
            stats.TokenUID += 1
            database.update_stats(txn, stats)
            token = Token()
            token.UID = stats.TokenUID
            token.ApplicationName = application_name.encode()
            token.OwnerID = owner_id.encode()
            database.token_put(txn, token)
        return token.as_dict, 201
    else:
        return {"error": "missing field"}, 400


@app.route('/v1.0/token/<uid>',  methods=["GET"])
@requires_auth
@limiter.exempt
def get_token_uid(uid: int):
    if not isinstance(uid, int):
        return {}, 400
    if not requires_scope("read:token"):
        return {}, 401
    with database.read_txn() as txn:
        token = database.token_get(txn, uid)
        if token is not None:
            return token.as_dict, 200
    return {}, 404


@app.route('/v1.0/pixel',  methods=["POST"])
@requires_auth
@limiter.exempt
def generate_pixel():
    if not requires_scope("create:pixel"):
        return {"error": "not authorized"}, 401
    if owner_id := request.json.get("OwnerID"):
        with database.write_txn() as txn:
            stats = database.get_stats(txn)
            stats.PixelUID += 1
            database.update_stats(txn, stats)
            pixel = Pixel()
            pixel.UID = stats.PixelUID
            pixel.UUID = uuid.uuid4().bytes
            pixel.OwnerID = owner_id.encode()
            database.pixel_put(txn, pixel)
        return pixel.as_dict, 201
    else:
        return {"error": "missing field"}, 400



@app.route('/v1.0/pixel/<uid>',  methods=["GET"])
@requires_auth
@limiter.exempt
def get_pixel_uid(uid: int):
    if not isinstance(uid, int):
        return {}, 400
    if not requires_scope("read:pixel"):
        return {}, 401
    with database.read_txn() as txn:
        pixel = database.pixel_get(txn, uid)
        if pixel is not None:
            return pixel.as_dict, 200
    return {}, 404


@app.route('/v1.0/owner',  methods=["GET"])
@requires_auth
@limiter.exempt
def get_owners():
    if requires_scope("list:owner"):
        with database.read_txn() as txn:
            owners = list(database.owners_all(txn))
        return {"owners": owners}, 200
    return {}, 401


@app.route('/v1.0/owner/<owner_id>/pixel',  methods=["GET"])
@requires_auth
@limiter.exempt
def get_pixel_owner_uid(owner_id: str):
    if requires_scope("list:pixel"):
        with database.read_txn() as txn:
            pixels = database.pixel_get_by_owner(txn, owner_id)
        return {"pixels": [pixel.as_dict for pixel in pixels]}, 200
    return {}, 401


@app.route('/v1.0/owner/<owner_id>/token',  methods=["GET"])
@requires_auth
@limiter.exempt
def get_token_owner_uid(owner_id: str):
    if requires_scope("list:token"):
        with database.read_txn() as txn:
            tokens = database.token_get_by_owner(txn, owner_id)
        return {"tokens": [token.as_dict for token in tokens]}, 200
    return {}, 401


@app.route('/v1.0/owner/<owner_id>/pixel/<uuid:uuid>',  methods=["GET"])
@requires_auth
@limiter.exempt
def get_pixel_visits_owner_and_uuid(owner_id: str, uuid: uuid.UUID):
    visits = []
    if requires_scope("read:pixel"):
        with database.read_txn() as txn:
            pixels = database.pixel_get_by_owner(txn, owner_id)
            pixels_by_uuid = {pixel.UUID: pixel for pixel in pixels}
            if uuid.bytes in pixels_by_uuid:
                for visit in database.visit_all_for_pixeluid(txn, pixels_by_uuid[uuid.bytes].UID):
                    visits.append(visit.as_dict)
            else:
                return {}, 401
        return {"visits": visits}, 200
    return {}, 401


@app.route('/v1.0/owner/<owner_id>/token/<application_name>',  methods=["GET"])
@requires_auth
@limiter.exempt
def get_token_visits_owner_and_uid(owner_id: str, application_name):
    visits = []
    if not requires_scope("read:token"):
        return {}, 401
    with database.read_txn() as txn:
        tokens = database.token_get_by_owner(txn, owner_id)
        tokens_by_key = {token.ApplicationName.decode(): token for token in tokens}
        if application_name in tokens_by_key:
            for visit in database.visit_all_for_tokenuid(txn, tokens_by_key[application_name].UID):
                visits.append(visit.as_dict)
        else:
            return {}, 404
    return {"visits": visits}, 200



@app.route('/v1.0/owner/<owner_id>/pixel/<uuid:uuid>',  methods=["DELETE"])
@requires_auth
@limiter.exempt
def delete_pixel_by_owner_and_uid(owner_id: str, uuid: uuid.UUID):
    if not requires_scope("delete:pixel"):
        return {}, 401
    with database.write_txn() as txn:
        pixels = database.pixel_get_by_owner(txn, owner_id)
        pixels_by_uuid = {pixel.UUID: pixel for pixel in pixels}
        if uuid.bytes in pixels_by_uuid:
            database.pixel_delete(txn, pixels_by_uuid[uuid.bytes].UID)
            return {}, 200




@app.route('/v1.0/owner/<owner_id>/token/<application_name>', methods=["DELETE"])
@requires_auth
@limiter.exempt
def delete_token_by_owner_and_uid(owner_id: str, application_name: str):
    if not requires_scope("delete:token"):
        return {}, 401
    with database.write_txn() as txn:
        tokens = database.token_get_by_owner(txn, owner_id)
        tokens_by_application = {token.ApplicationName: token for token in tokens}
        if application_name in tokens_by_application:
            database.token_delete(txn, tokens_by_application[application_name].UID)
            return {}, 200




def handle_pixel_visit(pixel_uuid: str):
    with database.read_txn() as txn:
        pixel = database.pixel_get_by_uuid(txn, pixel_uuid)
    if pixel is not None:
        ip_addr = ip_address(get_ipaddr())
        if ip_addr.version == 6:
            ipv6_packed = struct.unpack("!BBBBBBBBBBBBBBBB", ip_addr)
        else:
            ipv6_packed = struct.unpack("!BBBBBBBBBBBBBBBB", b'\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\xff\xff'+ip_addr.packed)
        try:
            dnt = int(request.headers.get("DNT")) or False
        except:
            dnt = False
        if not dnt:
            operating_system = (request.user_agent.platform or "")[:16].encode()
            browser = (request.user_agent.browser or "")[:16].encode()
            language = (request.user_agent.language or "")[:16].encode()
        else:
            operating_system = b""
            browser = b""
            language = b""
        visit = VisitPixel(
            PixelUID=pixel.UID,
            Timestamp=int(datetime.now().timestamp()),
            IPAddr=ipv6_packed,
            DoNotTrack=dnt,
            OperatingSystem=operating_system,
            Browser=browser,
            Language=language
        )
        with database.write_txn() as txn:
            database.visit_put(txn, visit)


@app.route('/v1.0/pixel/visit/<pixel_uuid>', methods=["GET"])
def visit_pixel(pixel_uuid: str):
    handle_pixel_visit(pixel_uuid)
    return "", 404


@app.route('/v1.0/pixel/visit/<pixel_uuid>/found', methods=["GET"])
def visit_pixel_found(pixel_uuid: str):
    handle_pixel_visit(pixel_uuid)
    return app.send_static_file("pixel.png")


@app.route('/v1.0/owner/<owner_id>/token/<application_name>/visit', methods=["POST"])
@requires_auth
@limiter.exempt
def visit_token(owner_id: str, application_name: str):
    if requires_scope("visit:token"):
        jdata = request.json
        target = jdata.get("target")
        if target is None:
            return {"error": "invalid request"}, 400
        with database.read_txn() as txn:
            tokens = database.token_get_by_owner(txn, owner_id)
        tokens_by_application = {token.ApplicationName: token for token in tokens}
        if application_name.encode() in tokens_by_application:
            ip_net = ip_network(target)
            if ip_net.version == 6:
                ipv6_packed = struct.unpack("!BBBBBBBBBBBBBBBB", ip_net.network_address.packed)
            else:
                ipv6_packed = struct.unpack("!BBBBBBBBBBBBBBBB",
                                            b'\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\xff\xff' + ip_net.network_address.packed)
            visit = VisitToken(
                TokenUID=tokens_by_application[application_name.encode()].UID,
                Timestamp=int(datetime.now().timestamp()),
                IPAddr=ipv6_packed,
                IPPrefixLen=ip_net.prefixlen
            )
            with database.write_txn() as txn:
                database.visit_token_put(txn, visit)
            return "", 201
        return {"error": "not found"}, 404
    return {}, 401


if __name__ == '__main__':
    app.run()
